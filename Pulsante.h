//file header (.h) vengono richiamati i "prototipi"
//si inseriscono funzioni e variabili della classe

//includere tutto il contenuto del file.h all'interno del blocco
#ifndef Pulsante_h
#define Pulsante_h

class Pulsante {

    //indicatore di visibilità delle variabili,metodi o funzioni esterno alla classe
  public:

    //costruttore
    Pulsante(int buttonPin, int outPin);
    
    //variabili
    const int inputPin;
    const int outputPin;
    int buttonState;
    int lastButtonState;
    int outState;

    //funzione
    void premiPuls();
    

    //indicatore di visibilità delle variabili,metodi o funzioni interno alla classe
  private:

    //variabili
    unsigned long lastDebounceTime = 0;
    unsigned long debounceDelay = 50;

};

#endif // Pulsante_h
