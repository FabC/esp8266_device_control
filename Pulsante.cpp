#include "Pulsante.h"
#include "Arduino.h" //includere sempre nei file librerie o classi 
                     //oltre al nome della classe stessa

//costruttore della classe (metodo per inizializzare una istanza della classe)
//deve avere lo stesso nome della classe stessa
Pulsante::Pulsante(int buttonPin, int outPin):  //tra parentesi le proprietà del costruttore
inputPin(buttonPin), outputPin(outPin)          //inizializzazione delle proprità del costruttore
{
  pinMode(buttonPin, INPUT);
  pinMode(outPin, OUTPUT);
  digitalWrite(outPin, outState);
};

//metodo della classe (comportamento o azioni)
void Pulsante::premiPuls()
{
  int reading = digitalRead(inputPin);
  if (reading != lastButtonState) {
    // reset the debouncing timer
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
    
        if (reading != buttonState) {
      buttonState = reading;

     
      if (buttonState == HIGH) {
        outState = !outState;
      }
    }
  }

  
  digitalWrite(outputPin, outState);

 
  lastButtonState = reading;
}
  
